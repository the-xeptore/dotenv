# Fonts

## Colorful emojis
In order to make emojis colorful, copy `90-emoji.conf` to `/etc/fonts/conf.d/` as decribed [in official Arch wiki ](https://wiki.archlinux.org/index.php/Fonts#Force_color_emoji) and [here](https://www.reddit.com/r/archlinux/comments/52k3t0/proper_color_emoji_support/?utm_source=share&utm_medium=web2x).

