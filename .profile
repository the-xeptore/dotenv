# cargo
export PATH="$HOME/.cargo/bin:$PATH"

# GOPATH
export GOPATH="$HOME/dev/go/"
export GOBIN="$GOPATH/bin"

# X
export XDG_CONFIG_HOME="$HOME/.config"

# custom directories for easier access

# startX on tty1 if not already running
[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x Xorg >/dev/null && exec startx

