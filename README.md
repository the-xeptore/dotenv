# xeptore's .files
My desktop configuration dotfiles. From customized terminal to desktop and window manager status bar.

## Screenshot
![screenshot](https://gitlab.com/the-xeptore/dotfiles/uploads/89e6e52a79a2f632489d9f7022ab2cc8/screenshot.jpg)

## Installed Fonts
```sh
sudo pacman -Syu \
  adobe-source-sans-pro-fonts \
  awesome-terminal-fonts \
  unicode-emoji \
  otf-fira-code \
  otf-font-awesome \
  persian-fonts \ # you might not need this!
  ttf-bitstream-vera \
  ttf-dejavu \
  ttf-droid \
  ttf-fira-code \
  ttf-hack \
  ttf-inconsolata \
  ttf-joypixels \
  ttf-liberation \
  ttf-ms-fonts \
  ttf-roboto \
  ttf-ubuntu-font-family \
  ttf-wps-fonts
```

## Applications
- Official:
```sh
sudo pacman -Syu \
  alacritty \
  asciinema \
  chromium \
  clementine \
  cmatrix \
  cowsay \
  docker \
  dotnet-sdk \
  firefox-developer-edition \
  go \
  i3lock \
  jre8-openjdk \
  jdk8-openjdk \
  jdk11-openjdk \
  jdk10-openjdk \
  mpc \
  mpd \
  ncmpcpp \
  nim \
  nimble \
  noto-fonts \
  noto-fonts-emoji \
  php \
  powerline \
  powerline-fonts \
  ranger \
  rofi \
  rxvt-unicode \
  seahorse \
  termite \
  tmux \
  urxvt-perls \
  valgrind
```
  
- AUR:  
I use [trizen](https://github.com/trizen/trizen), but you can use whatever AUR package manager you want.
```sh
trizen -Syuc --noedit --noconfirm \
  brave-bin \
  brbullshit \
  brcatfish \
  brinsomnia \
  brvisual-studio-code-bin
```

