PS1='\[\033[1;32m\]\w\[\033[0m\] \[\033[1;37m\]λ\[\033[0m\] '

export HISTSIZE=4096
export HISTCONTROL=ignoreboth:erasedups

alias mv='mv -v'
alias cp='cp -rv'
alias g='git'
alias gst='g status'
alias gc='g commit --signoff'
alias cb='xclip -rmlastnl -selection clipboard'
alias rm='rm -v'
alias rimraf='rm -rf'

eval "$(rbenv init -)"

function switch_kubectl() {
    aws eks --region "$1" update-kubeconfig --name "$2"
}

function kubesb() {
    KUBECONFIG=~/.kube/sandbox.yml
    switch_kubectl eu-west-1 EKS-LD5A07m0IL3s
    export KUBECONFIG
}

function kubeuk() {
    KUBECONFIG=~/.kube/prod-uk.yml
    switch_kubectl eu-west-2 edge-prod-eks
    export KUBECONFIG
}

function kubeus() {
    KUBECONFIG=~/.kube/prod-us.yml
    switch_kubectl us-east-1 edge-us-prod-eks
    export KUBECONFIG
}

function mkcdir() {
    mkdir -p "$1"
    cd "$1"
}

function goup() {
    if [[ "$(go version | awk '{ print $3 }')" == "$(curl -s https://go.dev/VERSION?m=text)" ]]
    then
        echo 'latest version is already installed.'
    return 0
    fi

    rimraf -v "$GOPATH/go"

    curl -L "https://go.dev/dl/$(curl -s https://go.dev/VERSION?m=text).linux-amd64.tar.gz" | tar -xzf - -C "$GOPATH"
}

function cloudflaredup() {
    curl -L https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64 -o ~/.local/bin/cloudflared
    chmod +x ~/.local/bin/cloudflared
}
