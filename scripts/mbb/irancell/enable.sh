set -e

echo "Fetching token..."

token=$(sh /home/xeptore/.scripts/mbb/irancell/token.sh) || (echo 'Unable to fetch token.' && echo 'Exiting...' && exit 1)

echo "Token successfully fetched: $token"
echo "Disabling data..."

curl --connect-timeout 2 --retry 2 --retry-delay 1 -s -o /dev/null 'http://192.168.8.1/api/dialup/mobile-dataswitch' \
	-H 'Connection: keep-alive' \
	-H 'Pragma: no-cache' \
	-H 'Cache-Control: no-cache' \
	-H 'Accept: */*' \
	-H 'Origin: http://192.168.8.1' \
	-H 'X-Requested-With: XMLHttpRequest' \
	-H "__RequestVerificationToken: $token" \
	-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36' \
	-H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
	-H 'Referer: http://192.168.8.1/html/home.html' \
	-H 'Accept-Language: en-US,en;q=0.9' \
	--data '<?xml version="1.0" encoding="UTF-8"?><request><dataswitch>1</dataswitch></request>' \
	--compressed \
	--insecure || (echo 'Unable to enable data.' && echo 'Exiting...' && exit 1)

echo "Data enabled."
